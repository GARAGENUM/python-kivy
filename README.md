# CLIENT ANDROID POUR PYTHON API

Client Android pour API en python avec kivy (UI) et buildozer (build)

## PRE REQUIS :paperclip:

- Python3

## CONFIGURATION :wrench:

> Renseigner les champs api_key et api_url en fonction de votre configuration.

## UTILISATION :rocket:

- Installer les dépendances:
```bash
python3 -m pip install -r requirements.txt
```

- Lancer en local:
```bash
python3 main.py
```

:warning: Nécéssite une api_url fonctionnelle avec son api_key

### BUILD APK :iphone:

```bash
buildozer -v android debug
```

> Le fichier APK se trouve dans le dossier bin/

## TODO :bookmark_tabs:

- [X] Splashcreen
- [X] Make background random
- [X] Images in folder
- [X] Button CSS
- [ ] Lien code source
- [ ] Load API_KEY on startup if not found
