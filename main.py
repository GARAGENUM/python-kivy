import requests, json, os, random
from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivymd.app import MDApp
from kivymd.uix.button import MDRectangleFlatButton

# API key
api_key="A RECUPERER DU SERVEUR API"
api_url = "https://citations.domaine.tld/api/citations/random"


class MainApp(MDApp):
    def build(self):
        return MainWidget()

class MainWidget(BoxLayout):
    def __init__(self, **kwargs):
        super(MainWidget, self).__init__(orientation='vertical', **kwargs)
        self.cols = 1

        # citation
        self.quote_label = Label(text="", font_size='20sp')
        self.load_citation()
        self.add_widget(self.quote_label)

        # bouton
        button = MDRectangleFlatButton(text='Recharger', size_hint=(None, None), size=(300, 100), pos_hint={'center_x': 0.5, 'center_y': 0.1}, line_color="white", text_color="white")
        button.bind(on_press=self.load_citation)
        self.add_widget(button)

        # espace en dessous le bouton
        empty_label = Label(size_hint=(None, None), size=(1, 50))
        self.add_widget(empty_label)

    # for button
    def load_citation(self, instance=None):
        quote = get_citation(api_url)
        auteur = quote[1]
        citation = split_string(quote[0], 40)

        self.quote_label.text = citation + "\n\n -- " + auteur

        background_images = [f for f in os.listdir('./assets/img/') if f.endswith(('.png'))]
        if background_images:
            random_image = random.choice(background_images)
            self.canvas.before.children[1].source = os.path.join('./assets/img/', random_image)


# fonction pour récupérer la citation de l'API
def get_citation(api_url):

    try:
        headers = {
            "API-Key": api_key
        }

        response = requests.get(api_url, headers=headers)
        datas = json.loads(response.text)
        zen_quote = [datas["citations"]["citation"], datas["citations"]["auteur"]]

        # print(zen_quote[0])
        return zen_quote

    except Exception as e:
        print(f"Erreur lors de la requête API : {e}")
        return "pb" 

# fonction pour mettre en forme le texte 
def split_string(input_string, chunk_size):
    result = []
    while input_string:
        index = min(chunk_size, len(input_string))
        while index < len(input_string) and input_string[index] != ' ':
            index += 1

        result.append(input_string[:index])
        input_string = input_string[index:].lstrip()

    return "\n".join(result)


# main
if __name__ == '__main__':
    app = MainApp()
    app.run()